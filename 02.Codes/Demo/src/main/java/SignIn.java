public class SignIn {
	ReadExcelFile file = new ReadExcelFile("./src/main/resources/Excel/INFO.xlsx");
	String StUser 	= file.getData(0, 0, 1);
	String LocUser 	= file.getData(0, 1, 1);
	String PbUser 	= file.getData(0, 2, 1);
	String Perf 	= file.getData(0, 3, 1);
	String InUser 	= file.getData(0, 4, 1);
	String VPass 	= file.getData(0, 5, 1);
	String InPass 	= file.getData(0, 6, 1);
	
	String FName 	= file.getData(0, 0, 3);
	String LName 	= file.getData(0, 1, 3);
	String Zip 		= file.getData(0, 2, 3);
	
	public SignIn() {
	}
	public String GetStandardUser() {
		return StUser;
	}
	public String GetLockedOutUser() {
		return LocUser;
	}
	public String GetProblemUser() {
		return PbUser;
	}
	public String GetPerfomance() {
		return Perf;
	}
	public String GetInvalidUser() {
		return InUser;
	}
	public String GetValidPass() {
		return VPass;
	}
	public String GetInvalidPass() {
		return InPass;
	}
	public String GetFirstName() {
		return FName;
	}
	public String GetLastName() {
		return LName;
	}
	public String GetPostalCode() {
		return Zip;
	}
}
