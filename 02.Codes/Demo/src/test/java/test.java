
import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class test {
	WebDriver driver;
	SignIn 	firstTest = new SignIn();
	TakesScreenshot Screen;
	File	Screensht;

	@Before
	public void  preparation() {
		System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://www.saucedemo.com/");
		Screen = ((TakesScreenshot)driver);
	}
	@Test
	public void TC_SauceLabs_Login_001(){
		driver.get("https://www.saucedemo.com/");
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetStandardUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetValidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
		assertEquals(driver.findElement(By.xpath("//span[@class='title']")).getText(),"PRODUCTS");
		System.out.println("TC_SauceLabs_Login_001 verify");
		driver.quit();
	}
	@Test
	public void TC_SauceLabs_Login_002() {
		driver.get("https://www.saucedemo.com/");
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetStandardUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetInvalidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
		assertEquals(driver.findElement(By.xpath("//h3[@data-test='error']")).getText(),"Epic sadface: Username and password do not match any user in this service");
		System.out.println("TC_SauceLabs_Login_002 verify");
		driver.quit();
	}
	@Test
	public void TC_SauceLabs_Login_003() {
		driver.get("https://www.saucedemo.com/");
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetInvalidUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetValidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
		assertEquals(driver.findElement(By.xpath("//h3[@data-test='error']")).getText(),"Epic sadface: Username and password do not match any user in this service");
		System.out.println("TC_SauceLabs_Login_003 verify");
		driver.quit();
	}
	@Test
	public void TC_SauceLabs_Login_004() {
		driver.get("https://www.saucedemo.com/");
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetInvalidUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetInvalidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
		assertEquals(driver.findElement(By.xpath("//h3[@data-test='error']")).getText(),"Epic sadface: Username and password do not match any user in this service");
		System.out.println("TC_SauceLabs_Login_004 verify");
		driver.quit();
	}
	@Test
	public void TC_SauceLabs_02BikeLigth_001(){
		//Login
		driver.get("https://www.saucedemo.com/");
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetStandardUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetValidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
	    //To order the price(Low to High)
	    driver.findElement(By.xpath("//select[@class='product_sort_container']")).click();
	    driver.findElement(By.xpath("//option[@value='lohi']")).click();
	    //To select the Sauce Labs Bike Light
	    driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-bike-light']")).click();
	    //Go to cart
	    driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
	    //Go to bike ligth
	    driver.findElement(By.xpath("//div[@class='inventory_item_name']")).click();
	    //Verify
		assertEquals(driver.findElement(By.xpath("//div[@class='inventory_details_name large_size']")).getText(),"Sauce Labs Bike Light");
		System.out.println("TC_SauceLabs_BikeLigth_001 verify");
		driver.quit();
	}
	@Test
	public void TC_SauceLabs_02Jacket_002(){
		//Login
		driver.get("https://www.saucedemo.com/");
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetStandardUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetValidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
	    //To order the price(HIGH to LOW)
	    driver.findElement(By.xpath("//select[@class='product_sort_container']")).click();
	    driver.findElement(By.xpath("//option[@value='hilo']")).click();
	    //To select the Sauce Labs Fleece Jacket
	    driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-fleece-jacket']")).click();
	    //To select the Sauce Labs Bike Light
	    driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-bike-light']")).click();
	    //Verify the two items selected
	  	assertEquals(driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText(),"2");
	    //Go to Fleece Jacket and remove it.
	    driver.findElement(By.xpath("//button[@id='remove-sauce-labs-fleece-jacket']")).click();
	    //Verify an item selected
	  	assertEquals(driver.findElement(By.xpath("//span[@class='shopping_cart_badge']")).getText(),"1");
		System.out.println("TC_SauceLabs_02Jacket_002 verify");
		driver.quit();
	}
	@Test
	public void TC_SauceLabs_03CheckOut_001() throws IOException{
		//Login
		driver.get("https://www.saucedemo.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@id='user-name']")).sendKeys(firstTest.GetStandardUser());
	    driver.findElement(By.xpath("//input[@id='password']")).sendKeys(firstTest.GetValidPass());
	    driver.findElement(By.xpath("//input[@id='login-button']")).click();
	    //To select the Sauce Labs Bike Light
	    driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-bike-light']")).click();
	    //To select the Fleece Jacket
	    driver.findElement(By.xpath("//button[@id='add-to-cart-sauce-labs-backpack']")).click();
	    //Go to cart
	    driver.findElement(By.xpath("//a[@class='shopping_cart_link']")).click();
	    //To do the checkout
	    driver.findElement(By.xpath("//button[@id='checkout']")).click();
	    //To introduce the personal information
	    driver.findElement(By.xpath("//input[@id='first-name']")).sendKeys(firstTest.GetFirstName());
	    driver.findElement(By.xpath("//input[@id='last-name']")).sendKeys(firstTest.GetLastName());
	    driver.findElement(By.xpath("//input[@id='postal-code']")).sendKeys(firstTest.GetPostalCode());
	    //Continue with the CheckOut
	    driver.findElement(By.xpath("//input[@id='continue']")).click();
	    driver.findElement(By.xpath("//button[@id='finish']")).click();
	    //Take a screenshot
	    Screensht = Screen.getScreenshotAs(OutputType.FILE);
	    FileUtils.copyFile(Screensht, new File ("./src/main/resources/Screenshots/Screen.png"));
	    //Verify an item selected
	  	assertEquals(driver.findElement(By.xpath("//h2[@class='complete-header']")).getText(),"THANK YOU FOR YOUR ORDER");
		System.out.println("TC_SauceLabs_03CheckOut_001 verify");
		driver.quit();
	}

	

}